#include "implementation.h"

#include <algorithms/matchings.hpp>
#include <multipoles.hpp>
#include <graphs.hpp>

#include <cassert>

using namespace ba_graph;


void ignoredVerticesSquare() {
    Graph g(createG());
    for (int j = 1; j < 5; ++j) {
        addV(g, createV(), j);
    }
    for (int k = 1; k < 5; ++k) {
        for (int j = 1; j < 5; ++j) {
            if (k < j)
                addE(g, g[k].v(), g[j].v());
        }
    }
    auto ignoredVertices = std::vector<Vertex>{g[1].v()};

    Graph G = copy_identical(g);
    deleteV(G, 1);
    //G is now g without ignored vertices

    assert(all_matchings(g, ignoredVertices) == all_matchings(G));
}

/*void ignoredVerticesDifferent(){
    Graph g(createG());
    for (int j = 1; j < 5; ++j) {
        addV(g, createV(), j);
    }
    addE(g, g[0].v(), g[1].v());
    addE(g, g[1].v(), g[2].v());
    addE(g, g[2].v(), g[3].v());
    addE(g, g[3].v(), g[0].v());

    auto ignoredVertices = std::vector<Vertex>{g[0].v(), g[1].v()};

    Graph G = copy_identical(g);
    deleteV(G, 0);
    deleteV(G, 1);

    assert(all_matchings(g, ignoredVertices) == all_matchings(G));
}*/

void ignoredVerticesPetersen() {
    Graph g(create_petersen());
    Multipole m = multipole_by_splitting(g, {}, std::vector<Edge>{g[0].find(Number(1))->e()}, 10);
    auto ignoredVertices = std::vector<Vertex>{g[0].v(), g[1].v()};

    Graph G = copy_identical(g);
    deleteV(G, 0);
    deleteV(G, 1);

    assert(all_matchings(g, ignoredVertices) == all_matchings(G));
}

void ignoredVerticesPetersen2() {
    Graph g(create_petersen());
    auto ignoredVertices = std::vector<Vertex>{g[0].v(), g[4].v()};

    Graph G = copy_identical(g);
    deleteV(G, 0);
    deleteV(G, 4);

    assert(all_matchings(g, ignoredVertices) == all_matchings(G));
}

int main()
{
    Graph G(empty_graph(4));
    assert(all_matchings(G).size() == 1);

    G = std::move(complete_graph(4));
    assert(all_matchings(G, 1).size() == 6);
    assert(all_matchings(G, 2).size() == 3);
    assert(all_matchings(G, 3).size() == 0);
    assert(all_matchings(G, 4).size() == 0);

    G = std::move(square_grid(3, 3));
    assert(all_matchings(G, 1).size() == unsigned(G.size()));

    G = std::move(create_petersen());
    assert(all_matchings(G, 5).size() == 6);

    ignoredVerticesSquare();
    ignoredVerticesDifferent();
    ignoredVerticesPetersen();
    ignoredVerticesPetersen2();

    return 0;
}
