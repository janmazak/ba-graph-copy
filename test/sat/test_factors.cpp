#include "implementation.h"

#include <io/print_nice.hpp>

#include <graphs.hpp>
#include <multipoles.hpp>
#include <sat/solver_cmsat.hpp>
#include <sat/exec_factors.hpp>
#include <graphs/stored_graphs.hpp>
#include <io.hpp>

#include <cassert>

using namespace ba_graph;

bool cb(std::vector<Edge> &, int *count) {
    (*count)++;
    return true;
}

inline std::pair<int, int> UncoveredEdges(const Graph& g, const std::set<Edge> pm_union, const int dangling)
{
    unsigned long coveredInnerEdges = 0;
    unsigned long coveredDanglingEdges = 0;
    for (auto e : pm_union)
    {
        if (g.find(e.v1())->degree() != 1 && 
            g.find(e.v2())->degree() != 1)
        {
            coveredInnerEdges++;
        } 
        else if (g.find(e.v1())->degree() != 1 || g.find(e.v2())->degree() != 1)
        {
            coveredDanglingEdges++;
        } else 
        {
            //it is an isolated edge, just skip
        }
    }
    auto innerEdges = g.size() - dangling;
    return std::make_pair(innerEdges - coveredInnerEdges, dangling - coveredDanglingEdges);
}

inline std::vector<std::pair<int, int>> MinUncoveredEdges(
    const Graph& g, 
    const std::vector<std::vector<Edge>> perfect_matchings,
    const int dangling) {

    unsigned long minCoveredInnerEdges = g.size();
    unsigned long minCoveredDanglingEdges = dangling;
    auto result = std::vector<std::pair<int, int>>{};
    auto pm_sets = std::vector<std::set<Edge>>();
    for (auto pm : perfect_matchings){
        pm_sets.emplace_back(pm.begin(), pm.end());
    }
    if (!perfect_matchings.empty()) {
        if (perfect_matchings.size() < 3) {
            std::set<Edge> pm_union;
            for (auto &m : pm_sets) {
                pm_union.merge(m);
            }
            std::cout<<"less than 3 pms!"<<std::endl;
            auto [innerUncovered, danglingUncovered] = UncoveredEdges(g, pm_union, dangling);
            result.push_back(std::make_pair(innerUncovered, danglingUncovered));
        } else {
            for (unsigned long long i = 0; i < pm_sets.size(); ++i) {
                for (unsigned long long j = i + 1; j < pm_sets.size(); ++j) {
                    for (unsigned long long k = j + 1; k < pm_sets.size(); ++k) {

                        std::set<Edge> union1;
                        std::set<Edge> union2;
                        std::set_union(std::begin(pm_sets[i]), std::end(pm_sets[i]),
                                       std::begin(pm_sets[j]), std::end(pm_sets[j]),
                                       std::inserter(union1, std::begin(union1)));
                        std::set_union(std::begin(pm_sets[i]), std::end(pm_sets[i]),
                                       std::begin(pm_sets[k]), std::end(pm_sets[k]),
                                       std::inserter(union2, std::begin(union2)));
                        union1.merge(union2);

                        auto [innerUncovered, danglingUncovered] = UncoveredEdges(g, union1, dangling);

                        if ((innerUncovered < minCoveredInnerEdges && danglingUncovered <= minCoveredDanglingEdges) ||
                            (danglingUncovered < minCoveredDanglingEdges && innerUncovered <= minCoveredInnerEdges))
                        {
                            minCoveredInnerEdges = innerUncovered;
                            minCoveredDanglingEdges = danglingUncovered;
                        } 
                    }
                }
            }

            result.push_back(std::make_pair(minCoveredInnerEdges, minCoveredDanglingEdges));

            for (unsigned long long i = 0; i < pm_sets.size(); ++i) {
                for (unsigned long long j = i + 1; j < pm_sets.size(); ++j) {
                    for (unsigned long long k = j + 1; k < pm_sets.size(); ++k) {

                        std::set<Edge> union1;
                        std::set<Edge> union2;
                        std::set_union(std::begin(pm_sets[i]), std::end(pm_sets[i]),
                                       std::begin(pm_sets[j]), std::end(pm_sets[j]),
                                       std::inserter(union1, std::begin(union1)));
                        std::set_union(std::begin(pm_sets[i]), std::end(pm_sets[i]),
                                       std::begin(pm_sets[k]), std::end(pm_sets[k]),
                                       std::inserter(union2, std::begin(union2)));
                        union1.merge(union2);

                        auto [innerUncovered, danglingUncovered] = UncoveredEdges(g, union1, dangling);

                        if (innerUncovered + danglingUncovered == minCoveredInnerEdges + minCoveredDanglingEdges &&
                            !(innerUncovered == minCoveredInnerEdges && danglingUncovered == minCoveredDanglingEdges))
                        {
                            result.push_back(std::make_pair(innerUncovered, danglingUncovered));
                        } 
                    }
                }
            }
        }
    }
    return result;
}

int main() {

    CMSatSolver solver;
    CMAllSatSolver allSolver;

    auto s = std::set({0, 1, 2, 3, 4, 5});
    auto subsets = all_subsets(s, 1);
    assert(subsets.size() == 6);
    for (auto set : subsets)
        assert(set.size() == 1);
    subsets = all_subsets(s, 4);
    assert(subsets.size() == 15);
    for (auto set : subsets)
        assert(set.size() == 4);

    Graph g = std::move(empty_graph(4));
    addE(g, Location(0, 1));
    addE(g, Location(1, 2));
    addE(g, Location(1, 3));
    addE(g, Location(2, 3));
    assert(has_perfect_matching_sat(solver, g));
    int c = 0;
    assert(perfect_matchings_enumerate_sat(allSolver, g, cb, &c));
    assert(c == 1);
	assert(perfect_matchings_list_sat(allSolver, g).size() == 1);
    deleteV(g, 0);
    assert(!has_perfect_matching_sat(solver, g));

    g = std::move(circuit(11));
    assert(!has_perfect_matching_sat(solver, g));
    c = 0;
    assert(!perfect_matchings_enumerate_sat(allSolver, g, cb, &c));
    assert(c == 0);

    g = std::move(circuit(8));
    assert(has_perfect_matching_sat(solver, g));
    c = 0;
    assert(perfect_matchings_enumerate_sat(allSolver, g, cb, &c));
    assert(c == 2);

    g = std::move(complete_graph(4));
    assert(has_perfect_matching_sat(solver, g));
    c = 0;
    assert(perfect_matchings_enumerate_sat(allSolver, g, cb, &c));
    assert(c == 3);

    g = std::move(create_petersen());
    assert(has_perfect_matching_sat(solver, g));
    c = 0;
    assert(perfect_matchings_enumerate_sat(allSolver, g, cb, &c));
    assert(c == 6);
    assert(perfect_matchings_list_sat(allSolver, g).size() == 6);
    deleteV(g, 0);
    assert(!has_perfect_matching_sat(solver, g));
    assert(perfect_matchings_list_sat(allSolver, g).size() == 0);

    g = std::move(complete_graph(5));
    assert(!has_kfactor_sat(solver, g, 1));
    assert(has_kfactor_sat(solver, g, 2));
    assert(!has_kfactor_sat(solver, g, 3));
    assert(has_kfactor_sat(solver, g, 4));
    assert(!has_kfactor_sat(solver, g, 5));
    assert(kfactors_list_sat(allSolver, g, 1, static_factory).size() == 0);
    assert(kfactors_list_sat(allSolver, g, 2, static_factory).size() == 12);
    assert(kfactors_list_sat(allSolver, g, 3, static_factory).size() == 0);
    assert(kfactors_list_sat(allSolver, g, 4, static_factory).size() == 1);
    assert(kfactors_list_sat(allSolver, g, 5, static_factory).size() == 0);

    g = std::move(create_petersen());
    assert(has_kfactor_sat(solver, g, 1));
    assert(has_kfactor_sat(solver, g, 2));
    assert(has_kfactor_sat(solver, g, 3));
    assert(!has_kfactor_sat(solver, g, 4));
    assert(kfactors_list_sat(allSolver, g, 1, static_factory).size() == 6);
    assert(kfactors_list_sat(allSolver, g, 2, static_factory).size() == 6);
    assert(kfactors_list_sat(allSolver, g, 3, static_factory).size() == 1);
    assert(kfactors_list_sat(allSolver, g, 4, static_factory).size() == 0);
    assert(kfactors_list_sat(allSolver, g, 5, static_factory).size() == 0);



    // ________________________________________________________


    /*g = std::move(create_petersen());
    auto pms_petersen = perfect_matchings_list_sat(allSolver, g);
    std::cout<<"start of petersen tests"<<" | number of PMs in Petersen: "<<pms_petersen.size()<<std::endl;

    auto uncovered = MinUncoveredEdges(g, pms_petersen);
    std::cout<<"Petersen 3PM coverage: "<<CoveragePercent3PM(g, uncovered)<<"%"<<std::endl;

    for(int i = 0; i < 10; i++)
    for(int j = i + 1; j < 10; j++)
    {
        std::cout<<"ignoring vertices: "<<i<<", "<<j<<std::endl;
        auto perfect_matchings = perfect_matchings_list_sat(allSolver, g, std::vector<Vertex>{g[i].v(), g[j].v()});
        auto count = MinUncoveredEdges(g, perfect_matchings);
        //std::cout<<"3PM coverage: "<<CoveragePercent3PM(g, count)<<"%"<<std::endl;
        std::cout<<"3PM coverage: "<<(g.size() - count)<<"/"<<g.size()<<std::endl;
        assert(perfect_matchings.size() == 8);
    }

    for(int i = 0; i < 10; i++)
    for(int j = i + 1; j < 10; j++)
    for(int k = j + 1; k < 10; k++)
    {
        std::cout<<"ignoring vertices: "<<i<<", "<<j<<", "<<k<<std::endl;
        auto perfect_matchings = perfect_matchings_list_sat(allSolver, g, std::vector<Vertex>{g[i].v(), g[j].v(), g[k].v()});
        auto count = MinUncoveredEdges(g, perfect_matchings);
        std::cout<<"3PM coverage: "<<(g.size() - count)<<"/"<<g.size()<<std::endl;
        
        assert(perfect_matchings.size() == 12);
    }*/

    /*for(int i = 0; i < 10; i++)
    {
        g = std::move(create_petersen());
        Multipole m = multipole_by_splitting(g, std::vector<Number>{i}, std::vector<Location>{}, 10);
        std::cout<<"splitting vertex: "<<i<<std::endl;

        std::cout<<"ignoring dangling vertices"<<std::endl;
        auto perfect_matchings = perfect_matchings_list_sat(allSolver, g, std::vector<Vertex>{
                g.find(Number(10))->v(),
                g.find(Number(11))->v(),
                g.find(Number(12))->v()});
        auto count = MinUncoveredEdges(g, perfect_matchings);
        std::cout<<"3PM coverage: "<<(g.size() - count)<<"/"<<g.size()<<std::endl;
        assert(perfect_matchings.size() == 8);
    }*/

    //vytvorit multipole cuttovanim kazdu z hran, ignorovat dangling vrcholy,
    //zistit kolko perfect matchingov pridava 

    Configuration cfg;
    cfg.load_from_string("{\"storage\": {\"dir\": \"../../resources/graphs\"}}");


    //
    //          C4G5
    //
    //
    auto sg = StoredGraphs::create<SnarkStorageDataC4G5>(cfg);

    auto orders = std::vector<int>{10, 18,/* 20, 22, 24, 26, 28, 30, 32, 34, 36*/};
    
    for(long unsigned int k = 0; k < orders.size(); k++)
    {
        auto order = orders[k];
        auto graphCount = sg->get_graphs_count(order);
        std::cout<<"Number of graphs with order "<<order<<": "<<graphCount<<std::endl;
        for(int i = 0; i < graphCount; i++)
        {
            Graph G(sg->get_graph(order, i));
            std::cout<<"==================== GRAPH "<<i<<" with order "<<order<<" ===================="<<std::endl; 

            auto pms = perfect_matchings_list_sat(allSolver, G);
            auto uncoveredVector = MinUncoveredEdges(G, pms, 0);
            std::cout<<"3PM uncovered [original graph]: "<<uncoveredVector[0].first<<"/"<<(G.size())<<std::endl;
            
            //2 -> split edge ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            std::cout<<"Connectivity 2 [splitting edge]"<<std::endl;
            //
            for(int j = 0; j < G.order(); j++)
            {
                std::vector<Number> neighbors = G[j].list(IP::all(), IT::n2());
                for(auto n: neighbors)
                {
                    Graph H = copy_identical(G);
                    Multipole m = multipole_by_splitting(H, {}, std::vector<Edge>{H[j].find(Number(n))->e()}, order);

                    auto danglingVertices = std::vector<Vertex>();
                    danglingVertices.push_back(H.find(Number(order    ))->v());
                    danglingVertices.push_back(H.find(Number(order + 1))->v());

                    auto perfect_matchings = perfect_matchings_list_sat(allSolver, H, danglingVertices);
                    auto uncoveredVector = MinUncoveredEdges(H, perfect_matchings, 2);
                    //std::cout<<"variants: "<<uncoveredVector.size()<<std::endl;
                    for (auto [innerUncovered, danglingUncovered] : uncoveredVector)
                        std::cout<<"3PM uncovered [multipole]: inner:"<<innerUncovered<<"/"<<(H.size() - 2)<<" | dangling:"<<danglingUncovered<<"/2"<<std::endl;
                    std::cout<<"~~~~~~"<<std::endl;
                }
            }
            //
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            //3 -> split vertex ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            std::cout<<"Connectivity 3 [splitting vertex]"<<std::endl;
            //
            for(auto &r : G)
            {
                Graph H = copy_identical(G);
                Multipole m = multipole_by_splitting(H, std::vector<Number>{r.n()}, std::vector<Location>{}, order);
                
                auto danglingVertices = std::vector<Vertex>();
                danglingVertices.push_back(H.find(Number(order    ))->v());
                danglingVertices.push_back(H.find(Number(order + 1))->v());
                danglingVertices.push_back(H.find(Number(order + 2))->v());

                auto perfect_matchings = perfect_matchings_list_sat(allSolver, H, danglingVertices);
                auto uncoveredVector = MinUncoveredEdges(H, perfect_matchings, 3);
                for (auto [innerUncovered, danglingUncovered] : uncoveredVector)
                    std::cout<<"3PM uncovered [multipole]: inner:"<<innerUncovered<<"/"<<(H.size() - 3)<<" | dangling:"<<danglingUncovered<<"/3"<<std::endl;
            }
            //
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            //4 -> split 2 edges or split 2 neighbours ~~~~~~~~~~~~
            std::cout<<"Connectivity 4 [splitting 2 neighbours]"<<std::endl;
            //
            for(auto &r : G)
            {
                std::vector<Number> neighbours = r.list(IP::all(), IT::n2());
                for(auto n: neighbours)
                {
                    Graph H = copy_identical(G);
                    Multipole m = multipole_by_splitting(H, std::vector<Number>{r.n(), n}, std::vector<Location>{}, order);
                    
                    auto danglingVertices = std::vector<Vertex>();
                    danglingVertices.push_back(H.find(Number(order    ))->v());
                    danglingVertices.push_back(H.find(Number(order + 1))->v());
                    danglingVertices.push_back(H.find(Number(order + 2))->v());
                    danglingVertices.push_back(H.find(Number(order + 3))->v());

                    //need to make sure to delete the isolated edge here!
                    for (auto inc : H.list(RP::all(), IP::primary()))
                    {
                        auto e = inc->e();
                        if (e.v1().is_null() && e.v2().is_null())
                        {
                            deleteE(H, e);
                            std::cout<<"Deleted isolated edge"<<std::endl;
                        }
                    }

                    auto perfect_matchings = perfect_matchings_list_sat(allSolver, H, danglingVertices);
                    auto uncoveredVector = MinUncoveredEdges(H, perfect_matchings, 4);
                    for (auto [innerUncovered, danglingUncovered] : uncoveredVector)
                        std::cout<<"3PM uncovered [multipole]: inner:"<<innerUncovered<<"/"<<(H.size() - 4)<<" | dangling:"<<danglingUncovered<<"/4"<<std::endl;
                }
            }
            //
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            //5 -> split vertex + edge

            //petersen multipole vertex split inserting

            //N2 4 pole co najviac nepokrytych hran
        }
    } 
    //return 0;

    //
    //          C4G4
    //
    //
    auto sg2 = StoredGraphs::create<SnarkStorageDataC4G4>(cfg);

    auto orders2 = std::vector<int>{10, 18, 20, 22,/* 24, 26*/};
    
    /*for(int k = 0; k < orders2.size(); k++)
    {
        auto graphCount = sg2->get_graphs_count(orders2[k]);
        std::cout<<"Number of graphs with order "<<orders2[k]<<": "<<graphCount<<std::endl;
        for(int i = 0; i < graphCount; i++)
        {
            Graph G(sg2->get_graph(orders2[k], i));
            auto perfect_matchings = perfect_matchings_list_sat(allSolver, G);
            auto count = MinUncoveredEdges(G, perfect_matchings);
            std::cout<<"3PM coverage: "<<(G.size() - count)<<"/"<<G.size()<<std::endl;
        }
    }*/ 

    return 0;
}
